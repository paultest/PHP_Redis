# PHP_Redis

## 1-1 课程内容概要
#### 1. 课程概要
Redis是php项目中使用到的主要缓存服务，主要是参考慕课网的《redis的入门与应用_笔记》，这里主要介绍：
* redis的安装使用
* PHP如何使用redis
* redis最常也是必知必会的五大数据结构
* 常用命令

## 1-2 什么是redis
#### 1. 什么是redis
* redis是远程的（redis有服务端和客户端，一般说redis通常指服务器）
* redis是基于内存的（数据以及数据结构都是存储在内存中的，所以读写速度非常快，性能比基于硬盘的MySQL快很多，但是非常吃内存）
* redis是非关系型数据库（本质上是数据库，但是和MySQL不一样，MySQL关系型数据库存储时必须定义数据字典，而redis不需要）

## 1-3 redis的应用场景
#### 1. redis的应用场景
* 缓存: 某些系统接口比较慢的时候，可以把一些数据放在Redis中缓存起来，下次取数据就不进行非常耗时的SQL操作了，直接从缓存中取数据，这个是提升系统性能最常用的方法之一
* 队列: Redis中有list接口，可以存储list，可以使用Push插入队列的元素， 使用 Pop来弹出退出队列的元素，push和pop操作保证了原子性的实现
* 数据存储: 所有的增删改查都是在Redis中进行，Redis有硬盘的持久化机制，定期进行存储，不需要借助MySQL进行数据存储，保证了数据的完整性和安全性

## 2-1 redis的安装环境
#### 1. 安装环境
我自己本地的Linux环境是Cent OS 7.2（LNMP），下载的redis版本是3.0.6

#### 2. 前提前提
Linux必须要安装gcc和tcl，具体安装方法这里不进行介绍

## 2-2 redis服务器端安装
#### 1. Linux安装步骤
* [官网](https://redis.io/)下载redis压缩包，官网下载的是tar.gz格式的
* 解压后进入到该目录
* 执行make操作（在该目录执行make命令，需要等待一段时间，执行完成后可以通过在该目录的src目录里面看是否有redis前缀的文件判断）
* 执行make install命令（完成之后可以通过which redis-server命令查看安装的路径）
* 先把解压后的redis目录中的redis.conf复制到其他的地方，然后修改复制出来的redis.conf，在这里把redis目录中的redis.conf复制到/home/abc/redis/下，然后修改该redis.conf

![Alt1](img/1.png)

把里面的daemonize no改为yes，表示是从在后台运行的
里面还有一个port 6379，根据需要修改端口，为了安全的话建议修改端口

* 启动redis-server
这里是通过自定义的redis配置（即复制出来的redis.conf的文件地址）来启动redis-server，命令为：
````
[root@www redis-3.0.6]# redis-server /home/abc/redis/redis.conf 
````

* 通过ps aux|grep redis-server查看是否启动

![Alt2](img/2.png)

## 2-3 redis客户端的安装
#### 1. redis客户端的安装
上面的步骤安装好了之后，redis-cli（客户端）默认也是一起安装好的，直接可以通过redi-cli命令进行登录
```
redis-cli -h 127.0.0.1 -p 6379
```

#### 2. 判断是否安装成功
可以通过info命令来输出redis的信息，包括版本，redis配置路径，端口等等

![Alt3](img/3.png)

## 3-1 redis的五种数据类型
#### 1. redis的数据类型
![Alt4](img/4.jpg)

## 3-2 string类型操作
#### 1. String的类型
![Alt5](img/5.jpg)

#### 2. 写入string
```
set key value
```
如图：

![Alt6](img/6.png)

#### 3. 读取string
```
get key
```
如图：

![Alt7](img/7.png)

#### 4. 对整数的value进行自增自减以及加减操作
```
incr key      // 自增
decr key      // 自减
incrby key 2  // 加2
decrby key 2  // 减2
```
如图：先设置一个value为13的string，key为string3，然后执行自增，再减去3

![Alt8](img/8.png)

## 3-3 list类型操作
#### 1. list的类型
list类型是一个有序的集合，可以从集合的左边（头部）或者是右边（尾部）进行推入或者是弹出元素等操作

注意：list集合里面的元素并不要求是唯一的，可以有多个元素是相同的

![Alt9](img/9.png)

#### 2. 推入元素
```
lpush list value1 [value2]  // 头部（左边）插入元素（可以多个）
rpush list value1 [value2]  // 尾部（右边）插入元素（可以多个）
```
如图：

![Alt10](img/10.png)

此时的list从左到右为：1 2 3 4 5 6 7 8 9 10

#### 3. 移除元素
```
lpop list // 移出并获取集合的第一个元素（左边第一个）
rpop list // 移出并获取集合的最后一个元素（右边第一个）
```
如图：

![Alt11](img/11.png)

此时的list从左到右为：3 4 5 6 7 8 9

#### 4. 获取集合的长度
```
llen list
```

如图：

![Alt12](img/12.png)

获取此时list的长度：7

#### 5. 通过索引获取列表中的元素
```
lindex list index
```

如图：

![Alt13](img/13.png)

查看第一个元素的值为：3

## 3-4 set类型操作
#### 1. set类型
set类型和list类型类似，不同的是set中的元素都是唯一的而且set元素不分左右即不分头尾

![Alt14](img/14.jpg)

#### 2. 插入元素
```
sadd set value
```

比如：

![Alt15](img/15.png)

如果set中已经有该value的话，那么会返回0，插不进去的

#### 3. 查看set中有多少元素
```
scard set
```

#### 4. 删除元素
```
srem set1 value
```

#### 5. 返回集合set中的所有元素
```
smembers set
```
如图：

![Alt16](img/16.png)

## 3-5 hash类型操作
#### 1. hash类型
hash类型也称为散列类型，使用键值对的结构

![Alt17](img/17.jpg)

#### 2. 定义一个hash类型
```
hset hash key value
```
如图为定义一个hash1表，里面的key1的值为12

![Alt18](img/18.png)

再往这个hash1表中定义key2和key3：

![Alt19](img/19.png)

#### 3. 获取hash表中指定字段的值
```
hget hash key
```
如图：

![Alt20](img/20.png)

#### 4. 获取hash表中字段的数量
```
heln hash
```
如图：

![Alt21](img/21.png)

#### 5. 获取hash表中多个字段的值
```
hmget hash key1 key2
```
如图：

![Alt22](img/22.png)

#### 6. 获取hash表中的所有字段
```
hkeys hash
```
如图：

![Alt23](img/23.png)

#### 7. 删除hash表中的一个或者多个字段
```
hdel hash key1 key2
```
如图：

![Alt24](img/24.png)

## 3-6 sort set类型操作
#### 1. sort set（有序集合）类型
sort set（有序集合）类型：和hash很相似，不同的是每个元素都会关联一个double类型的分数，redis正是通过这个分数来为每个元素进行从小到大的排序，有序集合里面的元素是唯一的，但是分数及score是可以重复的

![Alt25](img/25.jpg)

#### 2. 添加集合
添加一个zset1集合，并且设置分数10.1，value为val1
设置分数11.2，value为val2
设置分数9.2，value为val3
```
zadd zset1 10.1 val1
zadd zset1 11.2 val2
zadd zset1 9.2 val3
```

#### 3. 获取set集合中的元素数量
```
zcard zset1
```

#### 4. 获取排行
获取前三位分数的排行（从小到大）：
```
zrange zset1 0 2 withscores
```
如图：

![Alt26](img/26.png)

注意：前两行表示最小的score，后面类似。浮点数的存储是像图片中的一样存储的

#### 5. 获取某个排行
查看val1的排行：

![Alt27](img/27.png)

表示排在第二位

#### 6. 注意
如果有两个元素的score相同的话，则按照value来排序，比如：

![Alt28](img/28.png)

val2和val3的score是一样的，但是val2排在前面

## 4-1 PHP redis扩展安装环境
#### 1. 前提条件1
Linux已经安装了PHP

#### 2. 前提条件2
查看是否安装了phpize和php-config，可以通过which命令查看是否安装了，如果未安装的话，yum install php-devel即可

#### 3. 下载php-redis
```
wget https://github.com/phpredis/phpredis/archive/3.1.4.tar.gz
```

#### 4. 可以通过php -m查看安装的扩展
php -m命令可以列出当前已经安装的php扩展

## 4-2 PHP redis扩展安装
#### 1. 具体的安装步骤
a. 解压php的redis扩展包
```
tar -zxvf xxx.tar.gz
unzip xxx.zip
```

b. 进去到redis目录中执行phpize生成configure
```
phpize
```
如图：

![Alt29](img/29.png)

c. 执行configure（将配置文件放到指定位置）
```
./configure --with-php-config=/usr/bin/php-config
```

d. make操作（编译）
```
make install
```
如图：

![Alt30](img/30.png)

表示成功了

e. php.ini中加入redis扩展
先查看一下php.ini的位置，使用下面的命令可以查看php.ini的位置：
```
php --ini
```
如图：

![Alt31](img/31.png)

是在/etc/下的php.ini

编辑php.ini，在最后增加一行：extension = redis.so

f. 使用php -m查看是否安装成功

## 5-1 redis的连接操作
#### 1. PHP的phpredis文档地址
https://github.com/phpredis/phpredis

#### 2. 命名空间
Redis类的命名空间是根，即/，所以在实际开发中，要实例化redis类的时候，最好前面加上\，即 new \Redis()

#### 3. 命令
phpredis的命令和参数基本和redis.io中的实际命令对应

#### 4. 连接redis
```
<?php
/**
 * 连接redis
 */

// 实例化redis
$redis = new \Redis();

// 连接redis
$redis->connect('127.0.0.1', 6379);

```

## 5-2 string类型操作
#### 1. PHP设置string类型
```
<?php
/**
 * string类型操作
 */

// 连接redis
$redis = new \Redis();
$redis->connect('127.0.0.1', 6379);

// 先删除对应的key
$redis->delete('string1');

// 设置string的值
$redis->set('string1', 'val1');

// 获取string类型的值
$val = $redis->get('string1');
var_dump($val);

// 设置string的值为整数
$redis->set('string2', 4);

// 对string类型的整数进行加减操作
$redis->incr('string2', 2);

// 获取string类型的值
$val = $redis->get('string2');
var_dump($val);

```
运行：
```
val1
6
```

## 5-3 list类型操作
#### 1. PHP设置list类型
```
<?php
/**
 * list类型操作
 */

// 连接redis
$redis = new \Redis();
$redis->connect('127.0.0.1', 6379);

// 先删除对应的key
$redis->delete('list1');

// 在list中加入元素
$redis->lPush('list1', 'val1');
$redis->lPush('list1', 'val2');
$redis->lPush('list1', 'val3');

// 推出最右的元素并且读取出来
$val1 = $redis->rPop('list1');
var_dump($val1);

// 推出最右的元素并且读取出来
$val2 = $redis->rPop('list1');
var_dump($val2);

```
运行：
```
val1
val2
```

## 5-4 set类型操作
#### 1. PHP设置set类型
```
<?php
/**
 * set 类型操作
 */

// 连接redis
$redis = new \Redis();
$redis->connect('127.0.0.1', 6379);

// 先删除对应的key
$redis->delete('set1');

// 设置set集合的值
$redis->sAdd('set1', 'val1');
$redis->sAdd('set1', 'val2');
$redis->sAdd('set1', 'val3');
$redis->sAdd('set1', 'val3');

// 获取set集合中的数量
$number = $redis->sCard('set1');
var_dump($number);

// 获取set集合中的所有值
$val = $redis->sMembers('set1');
var_dump($val);

```
注意：第13行重复插入val3不生效的

运行：
```
int(3)
array(3) {
  [0]=>
  string(4) "val1"
  [1]=>
  string(4) "val2"
  [2]=>
  string(4) "val3"
}
```

## 5-5 hash类型操作
#### 1. PHP设置hash类型
```
<?php
/**
 * hash 类型操作
 */

// 连接redis
$redis = new \Redis();
$redis->connect('127.0.0.1', 6379);

// hash 操作

// 先删除对应的key
$redis->delete('hash1');

// 设置hash值
$redis->hSet('hash1', 'name', 'xiaoming');
$redis->hSet('hash1', 'age', '25');
$redis->hSet('hash1', 'gender', '1');

// 获取hash值中的某个key
$name = $redis->hGet('hash1', 'name');
var_dump($name);

// 获取hash值中的多个key
$val = $redis->hMGet('hash1', array('name', 'age', 'gender'));
var_dump($val);

```
运行：
```
string(8) "xiaoming"
array(3) {
  ["name"]=>
  string(8) "xiaoming"
  ["age"]=>
  string(2) "25"
  ["gender"]=>
  string(1) "1"
}
```

## 5-6 sort set类型操作
#### 1. PHP设置sort set类型
```
<?php
/**
 * sort set 类型操作
 */

// 连接redis
$redis = new \Redis();
$redis->connect('127.0.0.1', 6379);

// sort set 操作

// 先删除对应的key
$redis->delete('zset1');

// 在sort set中加入元素
$redis->zAdd('zset1', 100, 'xiaoming');
$redis->zAdd('zset1', 90, 'xiaohong');
$redis->zAdd('zset1', 930, 'xiaowagn');

// 从低到高输出sort set的排行
$res1 = $redis->zRange('zset1', 0, -1);
var_dump($res1);

// 从高到低输出sort set的排行
$res2 = $redis->zRevRange('zset1', 0, -1);
var_dump($res2);

```
运行：
```
array(3) {
  [0]=>
  string(8) "xiaohong"
  [1]=>
  string(8) "xiaoming"
  [2]=>
  string(8) "xiaowagn"
}
array(3) {
  [0]=>
  string(8) "xiaowagn"
  [1]=>
  string(8) "xiaoming"
  [2]=>
  string(8) "xiaohong"
}
```