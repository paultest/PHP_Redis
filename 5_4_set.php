<?php
/**
 * set 类型操作
 */

// 连接redis
$redis = new \Redis();
$redis->connect('127.0.0.1', 6379);

// 先删除对应的key
$redis->delete('set1');

// 设置set集合的值
$redis->sAdd('set1', 'val1');
$redis->sAdd('set1', 'val2');
$redis->sAdd('set1', 'val3');
$redis->sAdd('set1', 'val3');

// 获取set集合中的数量
$number = $redis->sCard('set1');
var_dump($number);

// 获取set集合中的所有值
$val = $redis->sMembers('set1');
var_dump($val);

/**
运行：
int(3)
array(3) {
[0]=>
string(4) "val1"
[1]=>
string(4) "val2"
[2]=>
string(4) "val3"
}
 */