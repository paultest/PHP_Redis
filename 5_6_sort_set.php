<?php
/**
 * sort set 类型操作
 */

// 连接redis
$redis = new \Redis();
$redis->connect('127.0.0.1', 6379);

// sort set 操作

// 先删除对应的key
$redis->delete('zset1');

// 在sort set中加入元素
$redis->zAdd('zset1', 100, 'xiaoming');
$redis->zAdd('zset1', 90, 'xiaohong');
$redis->zAdd('zset1', 930, 'xiaowagn');

// 从低到高输出sort set的排行
$res1 = $redis->zRange('zset1', 0, -1);
var_dump($res1);

// 从高到低输出sort set的排行
$res2 = $redis->zRevRange('zset1', 0, -1);
var_dump($res2);

/**
运行：
array(3) {
[0]=>
string(8) "xiaohong"
[1]=>
string(8) "xiaoming"
[2]=>
string(8) "xiaowagn"
}
array(3) {
[0]=>
string(8) "xiaowagn"
[1]=>
string(8) "xiaoming"
[2]=>
string(8) "xiaohong"
}
 */