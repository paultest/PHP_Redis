<?php
/**
 * hash 类型操作
 */

// 连接redis
$redis = new \Redis();
$redis->connect('127.0.0.1', 6379);

// hash 操作

// 先删除对应的key
$redis->delete('hash1');

// 设置hash值
$redis->hSet('hash1', 'name', 'xiaoming');
$redis->hSet('hash1', 'age', '25');
$redis->hSet('hash1', 'gender', '1');

// 获取hash值中的某个key
$name = $redis->hGet('hash1', 'name');
var_dump($name);

// 获取hash值中的多个key
$val = $redis->hMGet('hash1', array('name', 'age', 'gender'));
var_dump($val);

/*
运行：
string(8) "xiaoming"
array(3) {
  ["name"]=>
  string(8) "xiaoming"
  ["age"]=>
  string(2) "25"
  ["gender"]=>
  string(1) "1"
}
 */