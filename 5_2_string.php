<?php
/**
 * string类型操作
 */

// 连接redis
$redis = new \Redis();
$redis->connect('127.0.0.1', 6379);

// 先删除对应的key
$redis->delete('string1');

// 设置string的值
$redis->set('string1', 'val1');

// 获取string类型的值
$val = $redis->get('string1');
var_dump($val);

// 设置string的值为整数
$redis->set('string2', 4);

// 对string类型的整数进行加减操作
$redis->incr('string2', 2);

// 获取string类型的值
$val = $redis->get('string2');
var_dump($val);

/**
运行：
val1
val2
 */