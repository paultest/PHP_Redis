<?php
/**
 * list类型操作
 */

// 连接redis
$redis = new \Redis();
$redis->connect('127.0.0.1', 6379);

// 先删除对应的key
$redis->delete('list1');

// 在list中加入元素
$redis->lPush('list1', 'val1');
$redis->lPush('list1', 'val2');
$redis->lPush('list1', 'val3');

// 推出最右的元素并且读取出来
$val1 = $redis->rPop('list1');
var_dump($val1);

// 推出最右的元素并且读取出来
$val2 = $redis->rPop('list1');
var_dump($val2);

/**
运行：
val1
val2
 */